var url = "localhost:7000";
url=document.URL.split("/")[2];
var timeout =60000;
function reqPost(service,inParam,func){
    var
        inData=
            {serviceName: service,
                inParam: inParam};
    console.log(inParam);
    $.ajax({
        type: "POST",
        timeout: timeout,
        async: true,
        data: JSON.stringify(inParam),
        contentType: 'application/json',

        url: "http://" + url + "/"+service,

        success: function (data) {

            console.log(service + "|" + func + "outBody = [ " );
            console.log(data);
            console.log("]");
            window[func](data);
        },
        error: function (data) {
            console.log(service + "|" + func + "|out：" );
            console.log(data);
            alert("调用服务失败，请联系管理员！");
        }
    });
}
function reqGet(service,func){
    $.ajax({
        type: "GET",
        timeout: timeout,
        async: true,
        contentType: 'application/json',

        url: "http://" + url + "/"+service,

        success: function (data) {

            console.log(service + "|" + func + "outBody = [ " );
            console.log(data);
            console.log("]");
            window[func](data);
        },
        error: function (data) {
            console.log(service + "|" + func + "|out：" );
            console.log(data);
            alert("调用服务失败，请联系管理员！");
        }
    });
}