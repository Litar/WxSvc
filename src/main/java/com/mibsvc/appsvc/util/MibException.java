package com.mibsvc.appsvc.util;

/**
 * Created by Administrator on 2017/7/4.
 */
public class MibException extends Exception{
    private String errorCode="";
    private String errorMsg ="";
    public MibException(String errorCode, String errorMsg){
        this.errorCode=errorCode;
        this.errorMsg=errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
