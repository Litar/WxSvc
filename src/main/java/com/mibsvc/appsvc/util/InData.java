package com.mibsvc.appsvc.util;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public  class InData  implements  Serializable  {
    public String serviceName;
    public Map inParam;

}
