package com.mibsvc.appsvc.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Created by litarchain on 2017/7/6.
 */
public class Util {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Long UIDLong() {
        String uuid = Math.abs(UUID.randomUUID().hashCode()) + "";
        return Long.parseLong(uuid);
    }
    public static String UIDString() {
        String uuid = Math.abs(UUID.randomUUID().hashCode()) + "";
        return uuid;
    }


    /**
     * 日期格式化工具：date --> String
     * @param date        需要被格式化的date
     * @param dateFormat  你需要的日期格式
     * @return
     */
    public static String formatDateTime(Date date, String dateFormat) {
        if (date == null)
            return "";
        if (!isNotNullAndEmpty(dateFormat)) {
            dateFormat = DEFAULT_DATE_FORMAT;
        }
        DateFormat format = new SimpleDateFormat(dateFormat);
        return format.format(date);
    }

    /**
     * 日期格式化工具：String --> Date
     * @param aDate
     * @param partten
     * @return
     * @throws ParseException
     */
    public static Date parseDate(String aDate, String partten) throws ParseException {
        if ((aDate == null) || (aDate.equals("")))
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat(partten);
        Date date = sdf.parse(aDate);
        return date;
    }

    /**
     * 字符切割 等效于 spilt
     * @param str
     * @param delimiter
     * @return
     */
    public static ArrayList<String> doDelimiter(String str, String delimiter) {
        StringTokenizer st = new StringTokenizer(str, delimiter);
        ArrayList<String> result = new ArrayList<String>();
        String deStr = "";
        while (st.hasMoreTokens()) {
            deStr = st.nextToken();
            result.add(deStr);
        }
        return result;
    }

    /**
     * 判断当前当前对象不为null或空字符串
     * @param expstr
     * @return
     */
    public static boolean isNotNullAndEmpty(Object expstr) {
        boolean result = true;
        if(expstr == null) {
            result =false;
        }
        if(expstr.equals("")) {
            result = false;
        }
        return result;
    }
}
