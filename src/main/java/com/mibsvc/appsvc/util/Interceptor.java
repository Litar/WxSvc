package com.mibsvc.appsvc.util;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by litarchain on 2017/7/6.
 */
public class Interceptor extends HandlerInterceptorAdapter {
    private static final String[] IGNORE_URL = {"/Zjlm", "/req"};
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        boolean flag = false;
        String url = request.getServletPath().toString();

//        System.out.println("getContextPath="+request.getContextPath().toString());
//        System.out.println("getServletPath="+request.getServletPath().toString());
//        System.out.println("getPathInfo ="+request.getPathInfo().toString());
//        System.out.println("getRequestURL="+request.getRequestURL().toString());
//        System.out.println("getRequestURI="+request.getRequestURI().toString());

         //不拦截上面定义的路径
        for (String str : IGNORE_URL) {
            System.out.println("str="+str);
            if (url.contains(str)) {
                flag = true;
                request.getRequestDispatcher(request.getRequestURL().toString()).forward(request,response);
                break;
            }
        }
        if (!flag) {
            try{
                String user = request.getSession().getAttribute("account").toString();

            }catch (Exception e) {

                response.sendRedirect("Zjlm");
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }
}
