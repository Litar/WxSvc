package com.mibsvc.appsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@SpringBootApplication
public class AppsvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppsvcApplication.class, args);
    }
}
