package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
@Entity
public class SysUserinfoUsergroup extends Model implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user_id", columnDefinition = "CHAR(32) comment \"用户ID\"")
    public SysUserInfo sysUserInfo;

    @ManyToOne
    @JoinColumn(name = "user_group_id", columnDefinition = "CHAR(32) comment \"用户组ID\"")
    public SysUserGroup sysUserGroup;
}
