package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:14
 */
@Data
@Entity
public class SysMenu extends Model implements Serializable {

    @Column(columnDefinition = "varchar(200) comment \"菜单名\"")
    private String menuName;

    @Column(columnDefinition = "varchar(200) comment \"菜单别名\"")
    private String aliasName;

    @Column(columnDefinition = "char(32) comment \"父级ID\"")
    private String parentId;

    @Column(columnDefinition = "varchar(100) comment \"树路径\"")
    private String path;

    @Column(columnDefinition = "varchar(10) comment \"资源类型\"")
    private String type;

    @Column(columnDefinition = "varchar(255) comment \"菜单链接\"")
    private String href;

    @Column(columnDefinition = "varchar(50) comment \"打开方式\"")
    private String target;

    @Column(columnDefinition = "varchar(50) comment \"菜单图标\"")
    private String icon;

    @Column(length = 1, columnDefinition = "int(1) default 1")
    private Short isShow;

    @Column(length = 1, columnDefinition = "int(1) default 1")
    private Short disabled;

    @Column(columnDefinition = "varchar(255) comment \"菜单介绍\"")
    private String note;

    @Column(columnDefinition = "int(32) comment \"排序字段\"")
    private Integer location;

    @Column(columnDefinition = "boolean comment \"是否含有子节点\"")
    private String hasChildren;


    @OneToMany(mappedBy = "sysMenu")
    public Set<SysPowerMenu> sysPowerFiles = new HashSet<SysPowerMenu>(0);
}
