package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:18
 */
@Data
@Entity
public class SysOperationLog extends Model implements Serializable {

    @ManyToOne
    @JoinColumn(name = "operation_type_id", columnDefinition = "CHAR(32) comment \"操作类型ID\"")
    public SysOperation sysOperation;

    @Column(columnDefinition = "varchar(500) comment \"操作内容\"")
    private String operationContent;

    @ManyToOne
    @JoinColumn(name = "operation_user_id", columnDefinition = "CHAR(32) comment \"操作用户ID\"")
    public SysUserInfo sysUserInfo;

    @Column(columnDefinition = "int(32) comment \"操作时间\"")
    private Integer operationTime;

}
