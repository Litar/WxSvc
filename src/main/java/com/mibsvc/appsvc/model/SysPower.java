package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:17
 */
@Data
@Entity
public class SysPower extends Model implements Serializable {

    @Column(columnDefinition = "varchar(50) comment \"权限类型\"")
    private String powerType;

    @OneToMany(mappedBy = "sysPower")
    public Set<SysRolePower> sysRolePowers = new HashSet<SysRolePower>(0);

    @OneToMany(mappedBy = "sysPower")
    public Set<SysPowerMenu> sysPowerMenus = new HashSet<SysPowerMenu>(0);

    @OneToMany(mappedBy = "sysPower")
    public Set<SysPowerOperation> sysPowerOperations = new HashSet<SysPowerOperation>(0);

    @OneToMany(mappedBy = "sysPower")
    public Set<SysPowerFile> sysUserRoles = new HashSet<SysPowerFile>(0);
}
