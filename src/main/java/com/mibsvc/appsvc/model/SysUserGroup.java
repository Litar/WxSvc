package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Administrator
 */
@Entity
@Data
public class SysUserGroup extends Model implements Serializable {

    @Column(columnDefinition = "VARCHAR(30) comment \"用户组名\"")
    private String groupName;
    @OneToMany(mappedBy = "sysUserGroup")
    public Set<SysUserinfoUsergroup> sysUserinfoUsergroups = new HashSet<SysUserinfoUsergroup>(0);
    @OneToMany(mappedBy = "sysUserGroup")
    public Set<SysRoleUsergroup> sysUserRoles = new HashSet<SysRoleUsergroup>(0);

}