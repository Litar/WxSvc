package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:00
 */

@Entity
@Data
public class SysRole extends Model implements Serializable {

    @Column(columnDefinition = "VARCHAR(30) comment \"角色名\"")
    private String roleName;
    @Column(columnDefinition = "VARCHAR(30) comment \"角色英文名\"")
    private String roleEnName;
    @Column(columnDefinition = "VARCHAR(30) comment \"code\"")
    private String code;
    @Column(columnDefinition    = "VARCHAR(30) comment \"别名\"")
    private String aliasName;
    @Column(columnDefinition = "VARCHAR(30) comment \"是否可用\"")
    private String disabled;
    @Column(columnDefinition = "VARCHAR(30) comment \"类型\"")
    private String type;
    @Column(columnDefinition = "VARCHAR(30) comment \"描述\"")
    private String note;
    @Column(columnDefinition = "VARCHAR(30) comment \"是否系统数据\"")
    private String isSys ;
    @Column(columnDefinition = "VARCHAR(255) comment \"备注信息\"")
    private String remarks  ;
    @OneToMany(mappedBy = "sysRole")
    public Set<SysUserRole> sysUserRoles = new HashSet<SysUserRole>(0);
    @OneToMany(mappedBy = "sysRole")
    public Set<SysRoleUsergroup> sysRoleUsergroups = new HashSet<SysRoleUsergroup>(0);
    @OneToMany(mappedBy = "sysRole")
    public Set<SysRolePower> sysRolePowers = new HashSet<SysRolePower>(0);


}
