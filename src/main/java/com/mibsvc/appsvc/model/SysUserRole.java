package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:04
 */
@Data
@Entity
public class SysUserRole extends Model implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user_id", columnDefinition = "CHAR(32) comment \"user_id\"")
    public SysUserInfo sysUserInfo;

    @ManyToOne
    @JoinColumn(name = "role_id", columnDefinition = "CHAR(32) comment \"user_id\"")
    public SysRole sysRole;
}
