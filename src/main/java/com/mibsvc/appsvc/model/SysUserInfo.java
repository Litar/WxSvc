package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Administrator
 */
@Data
@Entity
public class SysUserInfo extends Model implements Serializable {

    @Column(columnDefinition = "VARCHAR(50) comment \"用户名\"")
    private String userName;

    @OneToMany(mappedBy = "sysUserInfo")
    public Set<SysUserinfoUsergroup> sysUserinfoUsergroups = new HashSet<SysUserinfoUsergroup>(0);

    @OneToMany(mappedBy = "sysUserInfo")
    public Set<SysUserRole> sysUserRoles = new HashSet<SysUserRole>(0);

    @OneToMany(mappedBy = "sysUserInfo")
    public Set<SysOperationLog> sysOperationLogs = new HashSet<SysOperationLog>(0);


}
