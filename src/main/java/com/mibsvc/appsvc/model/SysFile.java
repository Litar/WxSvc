package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:14
 */
@Data
@Entity
public class SysFile extends Model implements Serializable {

    @Column(columnDefinition = "varchar(100) comment \"文件名\"")
    private String fileName;

    @Column(columnDefinition = "varchar(200) comment \"文件路径\"")
    private String filePath;

    @OneToMany(mappedBy = "sysFile")
    public Set<SysPowerFile> sysPowerFiles = new HashSet<SysPowerFile>(0);
}
