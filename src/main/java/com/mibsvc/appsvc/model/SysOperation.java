package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:17
 */
@Data
@Entity
public class SysOperation extends Model implements Serializable {


    @Column(columnDefinition = "varchar(50) comment \"操作名称\"")
    private String operationName;

    @Column(columnDefinition = "varchar(20) comment \"操作代码\"")
    private String operationCode;

    @Column(columnDefinition = "varchar(100) comment \"拦截URL\"")
    private String interceptUrl;

    @Column(columnDefinition = "char(32) comment \"操作父ID\"")
    private String parentId;

    @OneToMany(mappedBy = "sysOperation")
    public Set<SysPowerOperation> sysUserinfoUsergroups = new HashSet<SysPowerOperation>(0);

    @OneToMany(mappedBy = "sysOperation")
    public Set<SysOperationLog> sysOperationLogs = new HashSet<SysOperationLog>(0);

}
