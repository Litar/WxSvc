package com.mibsvc.appsvc.model;

import com.mibsvc.appsvc.util.Model;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @Author: Cmb
 * @Date: 2019/7/18 15:15
 */
@Data
@Entity
public class SysPowerMenu extends Model implements Serializable {


    @ManyToOne
    @JoinColumn(name = "power_id", columnDefinition = "CHAR(32) comment \"power_id\"")
    public SysPower sysPower;

    @ManyToOne
    @JoinColumn(name = "menu_id", columnDefinition = "CHAR(32) comment \"menu_id\"")
    public SysMenu sysMenu;


}
