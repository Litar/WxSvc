package com.mibsvc.appsvc.req.dto;

import lombok.Data;

@Data
public class Api000011 {
	private String province;
	private String region;
}
