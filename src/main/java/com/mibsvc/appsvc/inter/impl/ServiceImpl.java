package com.mibsvc.appsvc.inter.impl;


import com.mibsvc.appsvc.inter.IService;
import com.mibsvc.appsvc.util.MibException;
import com.mibsvc.appsvc.util.Mjson;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2018/11/25.
 */
@Service("IService")
public class ServiceImpl implements IService {
    @Override
    public Mjson login(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson add(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson getList(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson find(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson delInfo(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson update(Mjson mjson) throws MibException {
        return null;
    }

    @Override
    public Mjson getStatistics(Mjson mjson) throws MibException {
        return null;
    }
}
