package com.mibsvc.appsvc.inter;


import com.mibsvc.appsvc.util.Mjson;

/**
 * Created by litarchain on 2017/6/13.
 */
public interface IService {



    Mjson login(Mjson mjson)throws Exception;
    Mjson add(Mjson mjson)throws Exception;
    Mjson getList(Mjson mjson)throws Exception;
    Mjson find(Mjson mjson)throws Exception;
    Mjson delInfo(Mjson mjson)throws Exception;
    Mjson update(Mjson mjson)throws Exception;
    Mjson getStatistics(Mjson mjson)throws Exception;

}
