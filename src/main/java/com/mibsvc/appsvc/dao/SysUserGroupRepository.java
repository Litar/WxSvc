package com.mibsvc.appsvc.dao;

import com.mibsvc.appsvc.model.SysUserGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SysUserGroupRepository extends JpaRepository<SysUserGroup,String> {

}
